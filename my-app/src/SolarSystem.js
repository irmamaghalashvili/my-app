import React from 'react';
import './SolarSystem.css';

function SolarSystem() {
  const Slider = () => {
    const changeTheme = ()=> {
      document.querySelector('body').classList.toggle('dark');
    };
    return (
      <button className="slider" id="slider" onClick={changeTheme}>dark</button>
    );
  };


  const title = React.createElement(
    'h1',
    {
      style: {
        color: '#999',
        fontSize: '19px'
      }
    },
    'Solar system planets'
  );

  const planets = (
    <ul className="planets-list">
      <li>Mercury</li>
      <li>Venus</li>
      <li>Earth</li>
      <li>Mars</li>
      <li>Jupiter</li>
      <li>Saturn</li>
      <li>Uranus</li>
      <li>Neptune</li>
    </ul>
  );

  return (
    <div>
      <Slider/>
      {title}
      {planets}
    </div>
  );
}

export default SolarSystem;
